import createPersistedState from 'vuex-persistedstate';
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)


const store = new Vuex.Store({
  state: {
    tableWidth:'',
    tableHeight:''
  },
  mutations: {
    changeTable(state,size){
      state.tableWidth=size[0]
      state.tableHeight=size[1]
    }
  },
  actions: {
  },
  modules: {
  }
});
// 使用 vuex-persistedstate 插件
const persistedStateOptions = {
  /* 配置选项 */
};
const persistedStatePlugin = createPersistedState(persistedStateOptions);
persistedStatePlugin(store);

export default store;

