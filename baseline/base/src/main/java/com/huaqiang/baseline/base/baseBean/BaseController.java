package com.huaqiang.baseline.base.baseBean;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huaqiang.baseline.base.pojo.vo.ResponseResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * 基础Controller
 *
 * @param <V>vo
 * @param <T>entity
 * @param <Q>query
 * @author crelle
 */
public interface BaseController<V, T, Q> {

    /**
     * 创建一个
     *
     * @param object
     * @return
     */
    ResponseResult<T> create(@RequestBody T object);

    /**
     * 创建多个
     *
     * @param objects
     * @return
     */
    ResponseResult<List<T>> creates(@RequestBody List<T> objects);

    /**
     * 根据id删除
     *
     * @param id
     * @return
     */

    ResponseResult<String> deleteById(@PathVariable String id);

    /**
     * 根据ID批量删除
     *
     * @param ids
     * @return
     */
    ResponseResult<String> deleteByIds(@RequestBody List<String> ids);

    /**
     * 根据ID修改
     *
     * @param object
     * @return
     */
    ResponseResult<String> updateById(@RequestBody T object);

    /**
     * 批量修改
     *
     * @param objects
     * @return
     */
    ResponseResult<String> updateByIds(@RequestBody List<T> objects);

    /**
     * 自动分页查询
     *
     * @param pageBean
     * @return
     */
    ResponseResult<Page<T>> page(@RequestBody Page<T> pageBean);

    /**
     * 使用mybatis plus手动分页，自定义查询语句
     *
     * @param pageBean
     * @return
     */
    ResponseResult<Page<V>> manualPage(@RequestBody Page<Q> pageBean);

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    ResponseResult<T> queryById(@PathVariable String id);

    /**
     * 根据id批量查询
     *
     * @param ids
     * @return
     */
    ResponseResult<List<T>> queryByIds(@PathVariable List<String> ids);

    /**
     * 根据id批量查询
     *
     * @param
     * @return
     */
    ResponseResult<List<T>> queryAll();


}
