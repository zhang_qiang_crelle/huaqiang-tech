package com.huaqiang.baseline.base.baseBean;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 基本Dao
 *
 * @param <V>vo
 * @param <Q>query
 * @author crelle
 */
public interface BaseDao<V, Q> {

    /**
     * 手动分页,自定义查询
     *
     * @param query
     * @return
     */
    List<V> manualPage(@Param("param") Q query);
}
