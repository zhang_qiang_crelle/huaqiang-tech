package com.huaqiang.baseline.base.pojo.query;

import lombok.Getter;
import lombok.Setter;

/**
 * 手动分页查询入参
 *
 * @param <T> 入参
 */
@Getter
@Setter
public class PageQuery<T> {

    protected long total;
    protected long size;
    protected T queryBody;
}
