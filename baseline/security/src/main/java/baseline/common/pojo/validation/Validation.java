package baseline.common.pojo.validation;

/**
 * validation分组校验标记类
 */
public class Validation {

    public interface POST {
    }

    public interface DELETE {
    }

    public interface PUT {
    }

    public interface GET {
    }
}
