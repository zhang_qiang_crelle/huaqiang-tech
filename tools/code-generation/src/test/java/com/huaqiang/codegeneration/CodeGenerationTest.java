package com.huaqiang.codegeneration;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.builder.CustomFile;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.fill.Column;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Unit test for simple App.
 */
public class CodeGenerationTest {

    //项目目录
    private final String basePath = System.getProperty("user.dir");

    //TODO 1、设置父包名
    private final String parentPackageName = "com.archermind";
    //模块名
    //TODO 2、设置模块名
    private final String moduleName = "jt";

    //TODO 3、设置mapper xml的路径
    private final String mapperXmlPath = "/src/main/resources/mapper";

    //文件作者
    private final String author = "张强";
    //数据库url
    private final String url = "jdbc:postgresql://58.240.18.170:12083/jt-db?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai";
    //数据库用户名
    private final String username = "postgres";
    //数据库密码
    private final String password = "Archer@post32";


    @Test
    public void test() {
        //1、配置数据源
        FastAutoGenerator.create(url, username, password)
                //2、全局配置
                .globalConfig(builder -> {
                    builder.author(author) // 设置作者名
                            .outputDir(basePath + "/src/main/java")   //设置输出路径
                            .commentDate("yyyy-MM-dd hh:mm:ss")   //注释日期
                            .dateType(DateType.ONLY_DATE)   //定义生成的实体类中日期的类型 TIME_PACK=LocalDateTime;ONLY_DATE=Date;
//                            .enableSwagger()   //开启 swagger 模式
                            .disableOpenDir();   //禁止打开输出目录，默认打开
                })
                //3、包配置
                .packageConfig(builder -> {
                    builder.parent(parentPackageName) // 设置父包名
                            .moduleName(moduleName)   //设置模块包名
                            .entity("pojo.entity")   //pojo 实体类包名
                            .service("service") //Service 包名
                            .serviceImpl("service.impl") // ***ServiceImpl 包名
                            .mapper("mapper")   //Mapper 包名
                            .xml("mapper")  //Mapper XML 包名
                            .controller("controller") //Controller 包名
                            .pathInfo(Collections.singletonMap(OutputFile.xml, basePath + mapperXmlPath));    //配置 **Mapper.xml 路径信息：项目的 resources 目录的 Mapper 目录下
                })
                // 使用自定义模板
                .templateConfig(builder -> {
                    builder.entity("/templates/entity.java")   // 自定义 Entity 模板
                            .mapper("/templates/mapper.java") // 自定义 mapper 模板
                            .xml("/templates/mapper.xml")
                            .service("/templates/service.java")   // 自定义 Service 模板
                            .serviceImpl("/templates/serviceImpl.java") // 自定义 ServiceImpl 模板
                            .controller("/templates/controller.java"); // 自定义 Controller 模板
                })
                //自定义配置
                .injectionConfig(injectConfig -> {
                    Map<String,Object> customMap = new HashMap<>();
                    customMap.put("customDtoPackage",parentPackageName+"."+moduleName+"."+"pojo.dto");
                    customMap.put("customQueryPackage",parentPackageName+"."+moduleName+"."+"pojo.query");
                    customMap.put("customVoPackage",parentPackageName+"."+moduleName+"."+"pojo.vo");
                    customMap.put("customBasePackage",parentPackageName+"."+moduleName);
                    injectConfig.customMap(customMap); //注入自定义属性
                    // 添加 Query.java 生成
                    injectConfig.customFile(new CustomFile.Builder()
                            .fileName("Query.java") //文件名称
                            .templatePath("templates/entityQuery.java.ftl") //指定生成模板路径
                            .packageName("pojo.query") //包名,自3.5.10开始,可通过在package里面获取自定义包全路径,低版本下无法获取,示例:package.entityDTO
                            .build());
                    // 添加 Vo.java 生成
                    injectConfig.customFile(new CustomFile.Builder()
                            .fileName("Vo.java") // VO 文件
                            .templatePath("templates/entityVo.java.ftl") // VO 对应的模板路径
                            .packageName("pojo.vo") // VO 的包路径
                            .build());
                    // 添加 Dto.java 生成
                    injectConfig.customFile(new CustomFile.Builder()
                            .fileName("Dto.java") // VO 文件
                            .templatePath("templates/entityDto.java.ftl") // VO 对应的模板路径
                            .packageName("pojo.dto") // VO 的包路径
                            .build());
                })
                //4、策略配置
                .strategyConfig(builder -> {
                    //TODO 5、设置表名
                    builder.addInclude("t_devices","t_device_attributes","t_device_data") // 设置需要生成的数据表名
                            .addTablePrefix("t_", "c_") // 设置过滤表前缀
                            //4.1、Mapper策略配置
                            .mapperBuilder().superClass(BaseMapper.class)   //设置父类
                            .enableFileOverride()
                            .formatMapperFileName("%sMapper")   //格式化 mapper 文件名称
                            .enableMapperAnnotation()       //开启 @Mapper 注解
                            .formatXmlFileName("%sMapper") //格式化 Xml 文件名称
                            .enableBaseColumnList()     //生成baseColumn
                            .enableBaseResultMap()      //生成baseResultMap
                            .enableMapperAnnotation()

                            //4.2、service 策略配置
                            .serviceBuilder().formatServiceFileName("%sService") //格式化 service 接口文件名称，%s进行匹配表名，如 UserService
                            .enableFileOverride()
                            .formatServiceImplFileName("%sServiceImpl") //格式化 service 实现类文件名称，%s进行匹配表名，如 UserServiceImpl

                            //4.4、Controller策略配置
                            .controllerBuilder().formatFileName("%sController") //格式化 Controller 类文件名称，%s进行匹配表名，如 UserController
                            .enableFileOverride()
                            .enableRestStyle() //开启生成 @RestController 控制器

                            //4.3、实体类策略配置
                            .entityBuilder().enableLombok() //开启 Lombok
                            .disableSerialVersionUID()  //不实现 Serializable 接口，不生产 SerialVersionUID
                            .enableFileOverride()
                            .idType(IdType.ASSIGN_UUID).logicDeleteColumnName("deleted")   //逻辑删除字段名
                            .naming(NamingStrategy.underline_to_camel)  //数据库表映射到实体的命名策略：下划线转驼峰命
                            .columnNaming(NamingStrategy.underline_to_camel)    //数据库表字段映射到实体的命名策略：下划线转驼峰命
                            .addTableFills(new Column("create_time", FieldFill.INSERT), new Column("modify_time", FieldFill.INSERT_UPDATE))   //添加表字段填充，"create_time"字段自动填充为插入时间，"modify_time"字段自动填充为插入修改时间
                            .enableTableFieldAnnotation();      // 开启生成实体时生成字段注解

                })

                // 使用 FreeMarker 模板引擎（默认是 Velocity）
                .templateEngine(new FreemarkerTemplateEngine())
                .execute();
    }

}
