package ${package.Controller};


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import com.huaqiang.baseline.base.baseBean.BaseController;
import com.huaqiang.baseline.base.pojo.vo.ResponseResult;
import com.huaqiang.baseline.base.exception.BusinessException;

import java.util.List;

import ${customBasePackage}.pojo.vo.${entity}Vo;
import ${customBasePackage}.pojo.entity.${entity};
import ${customBasePackage}.pojo.query.${entity}Query;
import ${customBasePackage}.service.${entity}Service;

<#if restControllerStyle>
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>

/**
 * ${table.comment!}接口
 *
 * @author ${author}
 * @since ${date}
 */
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping("<#if package.ModuleName?? && package.ModuleName != "">/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} {
<#else>
@Slf4j
public class ${table.controllerName} implements BaseController<${entity}Vo, ${entity}, ${entity}Query> {
</#if>

    @Autowired
    private ${entity}Service ${entity?lower_case}Service;

    /**
     * 创建${entity}
     *
     * @param object
     * @return
     */
    @Override
    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseResult<${entity}> create(${entity} object) {
        ResponseResult<${entity}> responseResult = new ResponseResult<>();
        try {
            ${entity?lower_case}Service.create(object);
            responseResult.setData(object);
        } catch (Exception e) {
            log.error("创建${entity}失败,e={}", e);
            throw new BusinessException("创建${entity}失败");
        }
        return responseResult;
    }

    /**
     * 批量创建${entity}
     *
     * @param objects
     * @return
     */
    @Override
    @RequestMapping(value = "/creates", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseResult<List<${entity}>> creates(List<${entity}> objects) {
        ResponseResult<List<${entity}>> responseResult = new ResponseResult<>();
        try {
            ${entity?lower_case}Service.create(objects);
            responseResult.setData(objects);
        } catch (Exception e) {
            log.error("批量创建${entity}失败,e={}", e);
            throw new BusinessException("批量创建${entity}失败");
        }
        return responseResult;
    }

    /**
     * 删除${entity}
     *
     * @param id
     * @return
     */
    @Override
    @RequestMapping(value = "/deleteById/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseResult<String> deleteById(@PathVariable("id") String id) {
        ResponseResult<String> responseResult = new ResponseResult<>();
        try {
            ${entity?lower_case}Service.deleteById(id);
        } catch (Exception e) {
            log.error("删除${entity}失败,e={}", e);
            throw new BusinessException("删除${entity}失败");
        }
        return responseResult;
    }

    /**
     * 批量删除${entity}
     *
     * @param ids
     * @return
     */
    @Override
    @RequestMapping(value = "/deleteByIds", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseResult<String> deleteByIds(@RequestBody List<String> ids) {
        ResponseResult<String> responseResult = new ResponseResult<>();
        try {
            ${entity?lower_case}Service.deleteByIds(ids);
        } catch (Exception e) {
            log.error("批量删除${entity}失败,e={}", e);
            throw new BusinessException("批量删除${entity}失败");
        }
        return responseResult;
    }

    /**
     * 更新${entity}
     *
     * @param object
     * @return
     */
    @Override
    @RequestMapping(value = "/updateById", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseResult<String> updateById(@RequestBody ${entity} object) {
        ResponseResult<String> responseResult = new ResponseResult<>();
        try {
            ${entity?lower_case}Service.update(object);
        } catch (Exception e) {
            log.error("更新${entity}失败,e={}", e);
            throw new BusinessException("更新${entity}失败");
        }
        return responseResult;
    }

    /**
     * 批量更新${entity}
     *
     * @param objects
     * @return
     */
    @Override
    @RequestMapping(value = "/updateByIds", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseResult<String> updateByIds(@RequestBody List<${entity}> objects) {
        ResponseResult<String> responseResult = new ResponseResult<>();
        try {
            ${entity?lower_case}Service.updates(objects);
        } catch (Exception e) {
            log.error("批量更新${entity}失败,e={}", e);
            throw new BusinessException("批量更新${entity}失败");
        }
        return responseResult;
    }


    /**
     * 自动分页查询${entity}
     *
     * @param pageBean
     * @return
     */
    @Override
    @RequestMapping(value = "/page", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseResult<Page<${entity}>> page(@RequestBody Page<${entity}> pageBean) {
        ResponseResult<Page<${entity}>> responseResult = new ResponseResult<>();
        try {
            Page<${entity}> page = ${entity?lower_case}Service.pageByCondition(pageBean);
            responseResult.setData(page);
        } catch (Exception e) {
            log.error("自动分页查询${entity}失败,e={}", e);
            throw new BusinessException("自动分页查询${entity}失败");
        }
        return responseResult;
    }

    /**
     * 手动分页查询${entity}
     *
     * @param pageBean
     * @return
     */
    @Override
    @RequestMapping(value = "/manualPage", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseResult<Page<${entity}Vo>> manualPage(@RequestBody Page<${entity}Query> pageBean) {
        ResponseResult<Page<${entity}Vo>> responseResult = new ResponseResult<>();
        try {
            Page<${entity}Vo> ${entity}VoPage = ${entity?lower_case}Service.manualPage(pageBean);
            responseResult.setData(${entity}VoPage);
        } catch (Exception e) {
            log.error("手动分页查询${entity}失败,e={}", e);
            throw new BusinessException("手动分页查询${entity}失败");
        }
        return responseResult;
    }

    /**
     * 根据id查询${entity}详情
     *
     * @param id
     * @return
     */
    @Override
    @RequestMapping(value = "/queryById/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseResult<${entity}> queryById(@PathVariable("id") String id) {
        ResponseResult<${entity}> responseResult = new ResponseResult<>();
        try {
            ${entity} ${entity} = ${entity?lower_case}Service.queryById(id);
        responseResult.setData(${entity});
        } catch (Exception e) {
            log.error("根据id查询${entity}详情失败,e={}", e);
            throw new BusinessException("根据id查询${entity}详情失败");
        }
        return responseResult;
    }

    /**
     * 根据id批量查询${entity}详情
     *
     * @param ids
     * @return
     */
    @Override
    @RequestMapping(value = "/queryByIds", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseResult<List<${entity}>> queryByIds(@RequestBody List<String> ids) {
        ResponseResult<List<${entity}>> responseResult = new ResponseResult<>();
        try {
            List<${entity}> ${entity} = ${entity?lower_case}Service.queryByIds(ids);
            responseResult.setData(${entity});
        } catch (Exception e) {
            log.error("根据id批量查询${entity}详情失败,e={}", e);
            throw new BusinessException("根据id批量查询${entity}详情失败");
        }
        return responseResult;
    }

    /**
     * 查询所有${entity}
     *
     * @return
     */
    @Override
    @RequestMapping(value = "/queryAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseResult<List<${entity}>> queryAll() {
        ResponseResult<List<${entity}>> responseResult = new ResponseResult<>();
        try {
            List<${entity}> ${entity} = ${entity?lower_case}Service.queryAll();
            responseResult.setData(${entity});
        } catch (Exception e) {
            log.error("查询所有${entity}失败,e={}", e);
            throw new BusinessException("查询所有${entity}失败");
        }
        return responseResult;
    }
}
</#if>
