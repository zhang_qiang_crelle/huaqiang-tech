package ${package.Service};


import com.huaqiang.baseline.base.baseBean.BaseService;
import ${package.Entity}.${entity};
import ${customBasePackage}.pojo.query.${entity}Query;
import ${customBasePackage}.pojo.vo.${entity}Vo;
import ${superServiceClassPackage};

/**
 * ${table.comment!} 服务类
 *
 * @author ${author}
 * @since ${date}
 */
<#if kotlin>
interface ${table.serviceName} : ${superServiceClass}<${entity}>
<#else>
public interface ${table.serviceName} extends ${superServiceClass}<${entity}>, BaseService<${entity}Vo, ${entity}, ${entity}Query> {

}
</#if>
