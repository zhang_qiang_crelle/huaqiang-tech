package ${package.ServiceImpl};


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import ${customBasePackage}.pojo.vo.${entity}Vo;
import ${customBasePackage}.pojo.query.${entity}Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ${package.Entity}.${entity};
import ${package.Mapper}.${table.mapperName};
import ${package.Service}.${table.serviceName};

import java.util.List;
/**
 * ${table.comment!} 服务实现类
 *
 * @author ${author}
 * @since ${date}
 */
@Service
<#if kotlin>
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>(), ${table.serviceName} {

}
<#else>
public class ${table.serviceImplName} extends ${superServiceImplClass}<${table.mapperName}, ${entity}> implements ${table.serviceName} {

    @Autowired
    private ${table.mapperName} ${entity?lower_case}Mapper;

    /**
     * 增加${entity}
     *
     * @param ${entity?lower_case}
     * @return
     */
    @Override
    public ${entity} create(${entity} ${entity?lower_case}) {
        super.save(${entity?lower_case});
        return ${entity?lower_case};
    }

    /**
     * 批量增加${entity}
     *
     * @param ${entity?lower_case}s
     * @return
     */
    @Override
    public List<${entity}> create(List<${entity}> ${entity?lower_case}s) {
        super.saveBatch(${entity?lower_case}s);
        return ${entity?lower_case}s;
    }

    /**
     * 根据id删除${entity}
     *
     * @param id
     * @return
     */
    @Override
    public boolean deleteById(String id) {
        return super.removeById(id);
    }

    /**
     * 根据id批量删除${entity}
     *
     * @param ids
     * @return
     */
    @Override
    public boolean deleteByIds(List<String> ids) {
        return super.removeBatchByIds(ids);
    }

    /**
     * 根据id修改${entity}
     *
     * @param ${entity?lower_case}
     * @return
     */
    @Override
    public boolean update(${entity} ${entity?lower_case}) {
        return super.updateById(${entity?lower_case});
    }

    /**
     * 根据id批量修改${entity}
     *
     * @param ${entity?lower_case}s
     * @return
     */
    @Override
    public boolean updates(List<${entity}> ${entity?lower_case}s) {
        return super.updateBatchById(${entity?lower_case}s);
    }

    /**
     * 自动分页查询${entity}，仅仅适用于单表查询
     *
     * @param page
     * @return
     */
    @Override
    public Page<${entity}> pageByCondition(Page<${entity}> page) {
        LambdaQueryWrapper<${entity}> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        if (null != page && page.getRecords().size() != 0) {
            ${entity} ${entity?lower_case} = page.getRecords().get(0);
            lambdaQueryWrapper.like(null != ${entity?lower_case}.getId(), ${entity}::getId, ${entity?lower_case}.getId());
        }
        return super.page(page, lambdaQueryWrapper);
    }

    /**
     * 手动分页查询${entity}，用于多表查询
     *
     * @param pageBean
     * @return
     */
    @Override
    public Page<${entity}Vo> manualPage(Page<${entity}Query> pageBean) {
        return null;
    }

    /**
     * 查询${entity}详情
     *
     * @param id
     * @return
     */
    @Override
    public ${entity} queryById(String id) {
        return super.getById(id);
    }

    /**
     * 根据id批量查询${entity}
     *
     * @param ids
     * @return
     */
    @Override
    public List<${entity}> queryByIds(List<String> ids) {
        return super.listByIds(ids);
    }

    /**
     * 查询所有${entity}
     *
     * @return
     */
    @Override
    public List<${entity}> queryAll() {
        return super.list();
    }
}
</#if>
